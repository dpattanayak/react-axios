import './App.css';
import ReactDOM from 'react-dom';
import axios from 'axios';

function App() {

  // AXIOS INSTANCE
  const axiosInstance = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com'
  });

  // GET REQUEST
  function getTodos(){
    axiosInstance.get('/todos', {
      timeout: 5000
    })
    .then(res => showOutput(res))
    .catch(err => console.error(err));
  }

  // POST REQUEST
  function addTodo(){
    axiosInstance.post('/todos', {
      title: 'New Todo',
      completed: false
    })
    .then(res => showOutput(res))
    .catch(err => console.error(err));
  }

  // PUT/PATCH REQUEST
  function updateTodo() {
    axiosInstance.patch('/todos/1', {
      title: 'Updated Todo',
      completed: true
    })
    .then(res => showOutput(res))
    .catch(err => console.error(err));
  }

  // DELETE REQUEST
  function removeTodo() {
    axiosInstance.delete('/todos/1')
    .then(res => showOutput(res))
    .catch(err => console.error(err));
  }

  // SIMULTANEOUS DATA
  function getData() {
    axios.all([
      axiosInstance.get('/todos?_limit=5'),
      axiosInstance.get('/posts?_limit=5')
    ])
    .then(axios.spread((todos, posts) => showOutput(posts)))
    .catch(err => console.error(err));
  }

  // CUSTOM HEADERS
  function customHeaders() {
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'sometoken'
      }
    };
    axiosInstance.post('/todos',{
        title: 'New Todo',
        completed: false
      },
      config
    )
    .then(res => showOutput(res))
    .catch(err => console.error(err));
  }

  // TRANSFORMING REQUESTS & RESPONSES
  function transformResponse() {
    const options = {
      method: 'post',
      url: '/todos',
      data: {
        title: 'Hello World'
      },
      transformResponse: axiosInstance.defaults.transformResponse.concat(data => {
        data.title = data.title.toUpperCase();
        return data;
      })
    };

    axiosInstance(options).then(res => showOutput(res));
  }

  // ERROR HANDLING
  function errorHandling() {
    axiosInstance.get('/todoss', {
      // validateStatus: function(status) {
      //   return status < 500; // Reject only if status is greater or equal to 500
      // }
    })
    .then(res => showOutput(res))
    .catch(err => {
      if (err.response) {
        // Server responded with a status other than 200 range
        console.log(err.response.data);
        console.log(err.response.status);
        console.log(err.response.headers);

        if (err.response.status === 404) {
          alert('Error: Page Not Found');
        }
      } else if (err.request) {
        // Request was made but no response
        console.error(err.request);
      } else {
        console.error(err.message);
      }
    });
  }

  // CANCEL TOKEN
  function cancelToken() {
    const source = axios.CancelToken.source();

    axiosInstance.get('/todos', {
        cancelToken: source.token
      })
      .then(res => showOutput(res))
      .catch(thrown => {
        if (axios.isCancel(thrown)) {
          console.log('Request canceled', thrown.message);
        }
      });

    if (true) {
      source.cancel('Request canceled!');
    }
  }

  // INTERCEPTING REQUESTS & RESPONSES
  axiosInstance.interceptors.request.use(
    config => {
      console.log(
        `${config.method.toUpperCase()} request sent to ${
          config.url
        } at ${new Date().toLocaleTimeString()}`
      );

      return config;
    },
    error => {
      return Promise.reject(error);
    }
  );

  axiosInstance.interceptors.response.use(
    response => {
      console.log(`Response received at ${new Date().toLocaleTimeString()}`,response.status);
      return response;
    },
    error => {
      return Promise.reject(error);
    }
  );




  function showOutput(res) {
    const element = (
      <code>
        <div class="card card-body mb-4">
          <h5>Status: {res.status}</h5>
        </div>

        <div class="card mt-3">
          <div class="card-header">
            Headers
          </div>
          <div class="card-body">
            <pre>{JSON.stringify(res.headers, null, 2)}</pre>
          </div>
        </div>

        <div class="card mt-3">
          <div class="card-header">
            Data
          </div>
          <div class="card-body">
            <pre>{JSON.stringify(res.data, null, 2)}</pre>
          </div>
        </div>

        <div class="card mt-3">
          <div class="card-header">
            Config
          </div>
          <div class="card-body">
            <pre>{JSON.stringify(res.config, null, 2)}</pre>
          </div>
        </div>
      </code>
    );
    ReactDOM.render(element, document.getElementById('res'));
  }

  return (
    <div className="App">
      <div class="container my-5">
        <div class="text-center">
          <h1 class="display-4 text-center mb-3">API Calls with AXIOS</h1>
          <button class="btn btn-primary my-3" onClick={getTodos}> GET </button>
          <button class="btn btn-info" onClick={addTodo}> POST </button>
          <button class="btn btn-warning" onClick={updateTodo}> PUT/PATCH </button>
          <button class="btn btn-danger" onClick={removeTodo}> DELETE </button>
          <button class="btn btn-secondary" onClick={getData}> Sim Requests </button>
          <button class="btn btn-secondary" onClick={customHeaders}> Custom Headers </button>
          <button class="btn btn-secondary" onClick={transformResponse}> Transform </button>
          <button class="btn btn-secondary" onClick={errorHandling}> Error Handling </button>
          <button class="btn btn-secondary" onClick={cancelToken}> Cancel </button>
        </div>
        <hr />
        <div id="res"></div>
      </div>
    </div>
  );
}

export default App;
